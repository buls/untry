<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>QTS Co-op</title>
		<link rel="stylesheet" href="css/style.css"/>
		<link rel="stylesheet" href="css/style2.css"/>
		<script src="js/script.js" type="text/javascript"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body>
	<div align="left" class="top_section" id="top_section">
		<div align="center" class="upper">
			<div align="left" class="width_hold">
				<div align="left" class="site_logo">
					<a href="#">
						<img src="images/logo.png" alt="QSMCSL" title="QSMCSL" height="40"/>
					</a>
				</div>
				<div align="right" class="acc_info">
					<ul class="navmenu">
						<li>
							<ul>
								<li class="desktop_link" style="border-right: 1px #ddd solid; border-width: 0 1px;">
								<a href="#">
									<i class="icon-home"></i>
									 &nbsp; My Dashboard
								</a>
								</li>
								<li class="desktop_link">
									<a href="tel:08035401600">
										<i class="icon-headphones" title="Customer Service"></i>
										&nbsp; 0803000000 (9PM-5PM)
									</a>
								</li>
								<li>
									<div id="menuhold">
										<a href="javascript:void(0);" id="menulink" onclick="showMenuList();">
											<img src="images/menu.png" alt="Menu Link" style="height: 30px; display: block"/>
										</a>
										<div align="center" id="menucontainer">
											<ul id="menulist" class="invisible">
												<li>
													<a href="#" id="menuclose" style="color: #fff; background: none; font-weight: bold;" onclick="showMenuList();">Close Menu</a>
												</li>
												<li>
													<a href="#">
														<i class="icon-home"></i>
														&nbsp; Home
													</a>
												</li>
												<li>
									<a href="tel:08035401600">
										<i class="icon-headphones" title="Customer Service"></i>
										&nbsp; 0803000000 (9PM-5PM)
									</a>
								</li>
												<li>
													<a href="#">
														<i class="icon-home"></i>
														&nbsp; My Dashboard
													</a>
												</li>
												<li>
													<a href="#">
														<i class="icon-home"></i>
														&nbsp; Loans
													</a>
												</li>
												<li>
													<a href="#">
														<i class="icon-home"></i>
														&nbsp; Savings
													</a>
												</li>
												<li>
													<a href="#">
														<i class="icon-off"></i>
														&nbsp; Settings
													</a>
												</li>
												<li>
													<a href="#">
														<i class="icon-off"></i>
														&nbsp; Contact Us
													</a>
												</li>
												<li>
													<a href="#">
														<i class="icon-off"></i>
														&nbsp; About
													</a>
												</li>
												<li>
													<a href="#">
														<i class="icon-off"></i>
														&nbsp; Log Out
													</a>
												</li>
												
											</ul>
										</div>
										<div></div>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div style="clear:both"></div>
		<div style="clear:both"></div>
	</div>