<%@include file="../includes/header.jsp" %>
<div align="center" class="middle_section">
	<div align="left" class="main_section">
		<div align="left" class="content">
				<div id="welcome" class="welcome">
					<div class="thehead" style="background: #ddeaf9">
						<p>Welcome: <b>voti</b></p>
					</div>
					<div id="thecontent" class="thecontent">
						<p><b>Surname</b>  Oti </p>
						<p><b>Firstname</b>  Vincent </p>
						<p><b>Email</b>  Oti@gmail.com <a href="#">Verify</a></a></p>
						<p><b>Phone</b>  08030000000 <a href="#">Verify</a></a></p>
					</div>
				</div>
				<div id="welcome_details" class="welcome_details">
					<div id="shares" class="shares">
						<div class="thehead" style="background: #ddeaf9">
							<p>Shares</p>
						</div>
						<div class="shares_details">
							<p>Opening Amount: N10,000.00</p>
							<p>Benefit: 
								4% of COOP profit / Opening Amount.
							</p>
						</div>
					</div>
					<div id="savings" class="savings">
						<div id="thehead" class="thehead" style="background: #ddeaf9">
							<div class="sleft">
								<p>Savings</p>
							</div>
							<div class="sright">
								<p>N6,00.00</p>
							</div>
						</div>
						<div class="shares_details">
							<table style="width:100%" id="t01">
							  <tr>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							</table>
						</div>
					</div>
					<div id="loans" class="loans">
						<div class="thehead" style="background: #ddeaf9">
							<div class="sleft">
								<p>Loans</p>
							</div>
							<div class="sright">
								<p>N1,500.00</p>
							</div>
						</div>
						<div class="shares_details">
							<table style="width:100%" id="t01">
							  <tr>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							  <tr>
							    <td>Eve</td>
							    <td>Jackson</td> 
							    <td>94</td>
							    <td>Jill</td>
							    <td>Smith</td> 
							    <td>50</td>
							  </tr>
							</table>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>

