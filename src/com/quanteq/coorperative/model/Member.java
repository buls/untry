package com.quanteq.coorperative.model;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(detachable="true")
public class Member {
	
	@PrimaryKey
	@Persistent
	private String email;
	@Persistent
	private String surname;	
	@Persistent
	private String firstname;
	@Persistent
	private String sex;
	@Persistent
	private String address;
	@Persistent
	private String phoneNumber;
	@Persistent
	private String password;
	@Persistent
	private String monthlySavings;
	@Persistent
	private boolean isPhoneNumberVerified;
	@Persistent
	private boolean isFirstTimeUser;
	@Persistent
	private boolean isEmailVerified;
	
	
	public Member() {
		this.isFirstTimeUser = true;
		this.monthlySavings = "0.0";
	}



	public Member(String surname, String firstname, String sex, 
			String address, String phoneNumber, String email,
			String password, String monthlySavings, String rating) {
		super();
		this.surname = surname;
		this.firstname = firstname;
		this.sex = sex;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.password = password;
		this.monthlySavings = monthlySavings;
	}



	public String getSurname() {
		return surname;
	}



	public void setSurname(String surname) {
		this.surname = surname;
	}



	public String getFirstname() {
		return firstname;
	}



	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}



	public String getSex() {
		return sex;
	}



	public void setSex(String sex) {
		this.sex = sex;
	}



	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public String getPhoneNumber() {
		return phoneNumber;
	}



	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	
	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getMonthlySavings() {
		return monthlySavings;
	}



	public void setMonthlySavings(String monthlySavings) {
		this.monthlySavings = monthlySavings;
	}



	public boolean isPhoneNumberVerified() {
		return isPhoneNumberVerified;
	}


	public void setPhoneNumberVerified(boolean isPhoneNumberVerified) {
		this.isPhoneNumberVerified = isPhoneNumberVerified;
	}
	
	
	
	public boolean isEmailVerified() {
		return isEmailVerified;
	}


	public void setEmailVerified(boolean isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}
	
	
	
	public boolean isFirstTimeUser() {
		return isFirstTimeUser;
	}


	public void setFirstTimeUser(boolean FirstTimeUser) {
		this.isFirstTimeUser = isFirstTimeUser;
	}
}
