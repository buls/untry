package com.quanteq.coorperative.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;


public class General extends HttpServlet {
		public void doGet(HttpServletRequest req, HttpServletResponse resp)
				throws IOException, ServletException {	
			String username = req.getParameter("username");
		    String password = req.getParameter("password");
		     
		    // do some processing here...
		     
		    /*// get response writer
		    PrintWriter writer = resp.getWriter();
		     
		    // build HTML code
		    String htmlRespone = "<html>";
		    htmlRespone += "<h2>Your username is: " + username + "<br/>";      
		    htmlRespone += "Your password is: " + password + "</h2>";    
		    htmlRespone += "</html>";
		     
		    // return response
		    writer.println(htmlRespone);*/
		    
		    
		    RequestDispatcher rd = req.getRequestDispatcher("WEB-INF/user_dashboard.jsp");
		    rd.forward(req,  resp);
			
}


public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
	// read form fields
    String username = req.getParameter("username");
    String password = req.getParameter("password");


/*    // do some processing here...

     
    // get response writer
    PrintWriter writer = resp.getWriter();
     
    // build HTML code
    String htmlRespone = "<html>";
    htmlRespone += "<h2>Your username is: " + username + "<br/>";      
    htmlRespone += "Your password is: " + password + "</h2>";    
    htmlRespone += "</html>";
     
    // return response

    writer.println(htmlRespone);
    */
    
    RequestDispatcher rd = req.getRequestDispatcher("WEB-INF/user_dashboard.jsp");
    rd.forward(req,  resp);

}


}
